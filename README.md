# RSRemoteConfig

Библиотека для подключения к сервису [RuStore Remote Config](https://remote-config.rustore.ru/).

Возможности:
- Обновление данных в фоне
- Кеширование

## Установка

### Swift Package Manager
 
1. Выберите ваш проект в Xcode
2. Добавьте SPM зависимость в Xcode

```swift
let package = Package( 
    dependencies: [
        .package(url: "https://gitflic.ru/project/rustore/rustore-remote-config-swift.git", from: "1.0.0")
    ],
    targets: [
        .target(
            name: "RuStore",
            dependencies: [
                .product(name: "RSRemoteConfig", package: "rustore-remote-config-swift")
            ]
        ) 
    ]
)
```

### Cocoapods

Добавьте библиотеку в `Podfile`:
```ruby
platform :iOS, '13.0'

target 'YourProjectName' do 
  use_frameworks!
  
  pod 'RSRemoteConfig', '-> 1.0.0'
end
```
Установите зависимости
```
pod install
```

## Использование

1. Скопируйте `appID` вашего приложения из консоли [https://remote-config.rustore.ru](https://remote-config.rustore.ru/) 
2. Импортируйте модуль `RSRemoteConfig`
3. Сконфигурируйте `RemoteConfigClient` для подключения к remote config серверу. 
4. Получите текущие значения remote config
```swift
import RSRemoteConfig

let client = RemoteConfigClient(appID: "ваш_app_id")
let remoteConfig = await client.remoteConfig()

let integerValue = remoteConfig.integer(forKey: "key_name")
```

Получение значений remote config происходит асинхронно.

Можно заранее "прогреть" конфигурацию с помощью вызова метода `start()`:
```swift
let client = RemoteConfigClient(appID: "ваш_app_id") 
client.start()
```

Необходимо удерживать сильную ссылку на объект `RemoteConfigClient` и везде использовать его для получения значений remote config.


## Конфигурация клиента

Клиент можно конфигурировать с помощью `RemoteConfigConfiguration`:
```swift
let configuration = RemoteConfigConfiguration(
    environment: .alpha,
    updateBehaviour: .actual, 
    requestParametersProvider: {
        RemoteConfigConfiguration.RequestParameters(
            account: item.account
        ) 
    }
)

let client = RemoteConfigClient(appID: "ваш_app_id", configuration: configuration)
```

### Параметры

#### `environment`

Используется для тестирования конфигурации на различных сборках приложения. Возможные значения: `.alpha`, `.beta`, `.release`.

#### `requestParametersProvider`

Замыкание, которое позволяет динамически передавать параметр account для получения remote config. Конфигурируется в [консоли](https://remote-config.rustore.ru).

#### `updateBehaviour`

Стратегия для фонового обновления remote config значений.

### Фоновые обновления

Полученные значения remote config кешируются локально и могут быть обновлены в фоне.

Существуют три стратегии фонового обновления значений remote config:
- Default (Используется по умолчанию) 
- Snapshot
- Actual

|                                                | **Default**       | **Snapshot**      | **Actual**        |
| ---------------------------------------------- | ------------------ | ----------------- | ----------------- |
| Фоновые обновления | ✅ Каждые 15 минут | ✅ Каждые 15 минут | ❌  |
| Кеширование значений | ✅  | ✅ | ❌ |
| Возвращает актуальные значения | ✅ Согласно интервалу обновления | ❌ Доступны после перезапуска клиента | ✅ |

#### Default

```swift
let configuration = RemoteConfigConfiguration( 
    updateBehaviour: .default(timeInterval: 900) // 15 mins
)

let client = RemoteConfigClient(appID: "ваш_app_id", configuration: configuration) 
let remoteConfig = await client.remoteConfig()
```

Если локальный кеш пуст или срок хранения кеша истек, то при вызове `await client.remoteConfig()`, будет отправлен запрос на сервер. Доступ к конфигурации будет ожидать завершения запроса. В обратном случае, будут возвращены значения из кеша.

Через 15 минут произойдет обновление значений remote config. Обновленные значения remote config будут сохранены в кеше, чтобы при следующем вызове `await client.remoteConfig()` вернуть свежие значения. Свежие значения будут автоматически применены ко всем объектам `RemoteConfig`, полученным ранее через `await client.remoteConfig()`

#### Snapshot

```swift
let configuration = RemoteConfigConfiguration(
    updateBehaviour: .snapshot(timeInterval: 900) // 15 mins
)

let client = RemoteConfigClient(appID: "ваш_app_id", configuration: configuration) 
let remoteConfig = await client.remoteConfig()
```

Выполняет фоновые обновления с указанным интервалом.

Интервал обновления задается в параметре `timeInterval` (по умолчанию — 15 минут).

Если локальный кеш пуст или срок хранения кеша истек, то при вызове `await client.remoteConfig()`, будет отправлен запрос на сервер. Доступ к конфигурации будет ожидать завершения запроса. В обратном случае, будут возвращены значения из кеша.

Через 15 минут произойдет обновление значений remote config. Обновленные значения remote config будут сохранены в кеше. Вызов `await client.remoteConfig()` будет всегда возвращать одинаковые значения в рамках жизни процесса.

⚠️ Отличие от `.default` в том, что обновленные значения remote config станут доступны после повторной инициализации клиента или перезапуска приложения.

#### Actual

```swift
let configuration = RemoteConfigConfiguration(
    updateBehaviour: .actual
)

let client = RemoteConfigClient(appID: "ваш_app_id", configuration: configuration) 
let remoteConfig = await client.remoteConfig()
```

Не запускает никаких обновлений в фоне.

При вызове `await client.remoteConfig()` всегда отправляет запрос на сервер, чтобы получить актуальные значения.

### Чтение значений remote config

После получения remote config, есть возможность читать типизированные значения:
```swift
let remoteConfig = await client.remoteConfig() 
remoteConfig.integer(forKey: "integer_value") 
remoteConfig.double(forKey: "double_value") 
remoteConfig.string(forKey: "string_value") 
remoteConfig.bool(forKey: "bool_value") 
remoteConfig.jsonObject(forKey: "json_object")
```

### Подписка на события

Можно получить сообщения о событиях в работе SDK с помощью объекта `RemoteConfigEventListener`:
```swift
let client = Client(appID: "ваш_app_id") 
client.addEventListener(
    RemoteConfigEventListener(
        didUpdatePersistentStorage: {
            print("remote config client did update persistent storage")
        },
        didCompleteNetworkRequest: { result in
            print("remote config client did complete network request with result \(String(describing: result))")
        }
    )
)
```

- `didUpdatePersistentStorage` — вызывается после обновления постоянного хранилища со значениями remote config
- `didCompleteNetworkRequest` — вызывается после окончания сетевого запроса

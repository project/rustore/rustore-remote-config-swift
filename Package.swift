// swift-tools-version:5.6

import PackageDescription

let package = Package(
    name: "RSRemoteConfig",
    platforms: [
        .macOS(.v11),
        .macCatalyst(.v14),
        .iOS(.v14)
    ],
    products: [
        .library(
            name: "RSRemoteConfig",
            targets: ["RSRemoteConfig"]
        )
    ],
    targets: [
        .binaryTarget(
            name: "RSRemoteConfig",
            url: "https://artifactory-external.vkpartner.ru/artifactory/rustore-swift/remote-config/1.0.0/RSRemoteConfig.xcframework.zip", 
            checksum: "c3807f8df238d095bc9e91f1be07a7d6fbd2ffc121695544fcb5404bf02f064c"
        )
    ],
    swiftLanguageVersions: [.v5]
)
